package com.cango.product.controller;

import com.cango.base.model.BaseResponse;
import com.cango.base.model.ProductEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.math.BigDecimal;

/**
 * @author liwenjie
 * @date 2021/10/29 14:34
 */
@RestController
@RequestMapping("/product")
public class ProductController {
    @GetMapping("/info")
    public BaseResponse<ProductEntity> getProductInfo(@RequestParam("applyCd")String applyCd){
        ProductEntity product = new ProductEntity(1,"超低息，贷无忧",new BigDecimal("1000000"),"1‰");
        return new BaseResponse(product);
    }
}
