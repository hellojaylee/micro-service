# 微服务DEMO

#### 文件目录树
```text
-- micro-service
|   |-- LICENSE
|   |-- README.md
|   |-- base //公共依赖
|   |-- ccs //ccs服务
|   |-- customer //客户信息服务
|   |-- gateway //网关
|   |-- nacos //注册中心安装包
|   |-- pom.xml
|   |-- product//产品服务
|   |-- sentinel-dashboard //sentinel链路追踪控制台 安装包
|   |-- doc
|       |-- 微服务.postman_collection.json //postman报文
--
```
#### 启动流程
+ [启动nacos](http://10.42.1.3:8848/nacos)
```shell script
cd micro-service/nacos-1.4.1/bin
./startup.sh -m standalone
tail -f /home/micro-service/nacos-1.4.1/logs/start.out
```
+ [启动sentinel](http://10.42.1.3:8088)
```shell script
cd ./sentinel
./service.sh start
./service.sh status
```
+ 代码编译、打包
```shell script
cd micro-service
mvn clean package
```
+ 启动服务
```shell script
java -Dfile.encoding=UTF-8 -jar gateway\target\gateway-0.0.1-SNAPSHOT.jar 
java -Dfile.encoding=UTF-8 -jar ccs\target\ccs-0.0.1-SNAPSHOT.jar
java -Dfile.encoding=UTF-8 -jar customer\target\customer-0.0.1-SNAPSHOT.jar
java -Dfile.encoding=UTF-8 -jar product\target\product-0.0.1-SNAPSHOT.jar
```
+ Postman导入 doc/微服务.postman_collection.json,进行微服务调用

   




