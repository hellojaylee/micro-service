package com.cango.base.model;

/**
 * @description: 
 * @author: liwenjie
 * @time: 2021/10/28 13:22
 */ 
public class BaseResponse<T> {

    private String code;

    private String msg;

    private T data;

    public BaseResponse() {
    }

    public BaseResponse(T data) {
        this.data = data;
        this.code = ErrorEnum.SUCCESS.getCode();
        this.msg = ErrorEnum.SUCCESS.getMsg();
    }

    public BaseResponse(String code, String msg) {
        super();
        this.code = code;
        this.msg = msg;
    }

    public BaseResponse(String code, String msg, T data) {
        super();
        this.code = code;
        this.msg = msg;
        this.data = data;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getMsg() {
        return msg;
    }

    public void setMsg(String msg) {
        this.msg = msg;
    }

    public T getData() {
        return data;
    }

    public void setData(T data) {
        this.data = data;
    }

    public static BaseResponse ok() {
        return new BaseResponse(ErrorEnum.SUCCESS.getCode(), ErrorEnum.SUCCESS.getMsg());
    }

    public static BaseResponse fail(String code, String msg) {
        return new BaseResponse(code, msg);
    }

    public static BaseResponse failure() {
        return new BaseResponse(ErrorEnum.OTHER_ERROR.getCode(), ErrorEnum.OTHER_ERROR.getMsg());
    }
}
