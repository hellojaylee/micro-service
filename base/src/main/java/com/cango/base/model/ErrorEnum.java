package com.cango.base.model;

/**
 * @Author: Ajax
 * @Date: 2018/8/6 12:16
 */
public enum ErrorEnum {
    SUCCESS("000", "success"),
    NO_AUTH("001","auth fail"),
    PARAM_ERROR("003","params error"),
    OTHER_ERROR("004","other");


    private String code;
    private String msg;

    public String getCode() {
        return code;
    }

    public String getMsg() {
        return msg;
    }

    ErrorEnum(String code, String msg) {
        this.code = code;
        this.msg = msg;
    }
}
