package com.cango.base.model;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.math.BigDecimal;

/**
 * @author liwenjie
 * @date 2021/10/29 14:18
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
public class ProductEntity {
    private Integer id;
    private String productName;
    private BigDecimal limit;
    private String rate;
}
