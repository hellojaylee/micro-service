package com.cango.base.model;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * @author liwenjie
 * @date 2021/10/29 14:16
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
public class CustomerEntity {
    private String idCard;
    private String name;
    private Integer age;
    private String tel;
    private String address;
}
