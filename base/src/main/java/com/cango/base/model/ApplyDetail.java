package com.cango.base.model;

import lombok.Data;

/**
 * @author liwenjie
 * @date 2021/10/29 14:29
 */
@Data
public class ApplyDetail {
    private CustomerEntity customer;
    private ProductEntity product;
}
