package com.cango.base.model;

import com.alibaba.fastjson.annotation.JSONField;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * @author liwenjie
 * @date 2021/8/27 14:17
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
public class ServiceInfo {
    @JSONField(name = "服务名",ordinal = 1)
    private String name;
    @JSONField(name = "端口号",ordinal = 2)
    private Integer port;
    @JSONField(name = "备注",ordinal = 3)
    private String comment;
}
