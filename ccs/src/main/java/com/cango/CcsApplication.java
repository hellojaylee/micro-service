package com.cango;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;
import org.springframework.cloud.openfeign.EnableFeignClients;

@SpringBootApplication
@EnableDiscoveryClient
@EnableFeignClients({"com.cango.ccs.feign"})
public class CcsApplication {

    public static void main(String[] args) {
        SpringApplication.run(CcsApplication.class, args);
    }

}
