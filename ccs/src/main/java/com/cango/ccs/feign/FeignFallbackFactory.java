package com.cango.ccs.feign;

import feign.hystrix.FallbackFactory;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

/**
 * @Description feign异常工厂类
 * @Author jaylee
 * @Date2020/8/12 15:10
 **/

@Component
@Slf4j
public class FeignFallbackFactory implements FallbackFactory<CustomerFeign> {
    @Autowired
    FeignFallbackHandler fallbackHandler;
    @Override
    public CustomerFeign create(Throwable throwable) {
        log.error("feign消费异常",throwable);
        return fallbackHandler;
    }
}
