package com.cango.ccs.feign;

import com.cango.base.model.BaseResponse;
import com.cango.base.model.CustomerEntity;
import org.springframework.stereotype.Component;

import java.util.Map;

/**
 * @description:
 * @author: liwenjie
 * @time: 2021/10/29 18:23
 */
@Component
public class FeignFallbackHandler implements CustomerFeign {
    @Override
    public BaseResponse<CustomerEntity> getCustomerInfo(String applyCd) {
        return BaseResponse.failure();
    }
}
