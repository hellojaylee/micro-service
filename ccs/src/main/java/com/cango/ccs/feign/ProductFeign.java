package com.cango.ccs.feign;
import com.cango.base.model.BaseResponse;
import com.cango.base.model.CustomerEntity;
import com.cango.base.model.ProductEntity;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;


/**
 * @author liwenjie
 * @date 2021/10/29 14:33
 */
@FeignClient(value = "product")
public interface ProductFeign {

    @GetMapping(value = "/product/info")
    BaseResponse<ProductEntity> getProductInfo(@RequestParam("applyCd") String applyCd);
}
