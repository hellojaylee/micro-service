package com.cango.ccs.feign;
import com.cango.base.model.BaseResponse;
import com.cango.base.model.CustomerEntity;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.stereotype.Component;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;


/**
 * @author liwenjie
 * @date 2021/10/29 14:33
 */
@FeignClient(value = "customer",fallbackFactory = FeignFallbackFactory.class )
@Component
public interface CustomerFeign {

    @GetMapping(value = "/customer/info")
    BaseResponse<CustomerEntity> getCustomerInfo(@RequestParam("applyCd") String applyCd);
}
