package com.cango.ccs.controller;

import com.cango.base.model.ApplyDetail;
import com.cango.base.model.BaseResponse;
import com.cango.base.model.ServiceInfo;
import com.cango.ccs.service.CcsService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;


/**
 * @author liwenjie
 * @date 2021/8/27 14:09
 */
@RestController
@RequestMapping("/ccs")
public class CcsController {
    @Autowired
    CcsService ccsService;
    @GetMapping(value = "/info",produces = {"application/json"})
    public BaseResponse<ServiceInfo> getServiceInfo(){
        return new BaseResponse(ccsService.sayHello());
    }

    @GetMapping("/applyDetail")
    public BaseResponse<ApplyDetail> getApplyDetail(@RequestParam("applyCd") String applyCd){
        return new BaseResponse(ccsService.getApplyDetail(applyCd));
    }
}
