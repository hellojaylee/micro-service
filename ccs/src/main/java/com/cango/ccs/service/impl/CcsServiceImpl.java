package com.cango.ccs.service.impl;

import com.alibaba.csp.sentinel.annotation.SentinelResource;
import com.cango.base.model.ApplyDetail;
import com.cango.base.model.CustomerEntity;
import com.cango.base.model.ProductEntity;
import com.cango.base.model.ServiceInfo;
import com.cango.ccs.feign.CustomerFeign;
import com.cango.ccs.feign.ProductFeign;
import com.cango.ccs.service.CcsService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;

/**
 * @author liwenjie
 * @date 2021/10/15 10:10
 */
@Service
public class CcsServiceImpl implements CcsService {
    @Value("${spring.application.name:unknown}")
    private String serviceName;
    @Value("${server.port}")
    private Integer port;
    @Value("${spring.application.desc}")
    private String content;

    @Autowired
    CustomerFeign customerFeign;

    @Autowired
    ProductFeign productFeign;

    @Override
    @SentinelResource("resource")
    public ServiceInfo sayHello() {
        return new ServiceInfo(serviceName,port,content);
    }

    @Override
    @SentinelResource("resource")
    public ApplyDetail getApplyDetail(String applyCd) {
        ApplyDetail detail = new ApplyDetail();
        CustomerEntity customer = customerFeign.getCustomerInfo(applyCd).getData();
        ProductEntity product = productFeign.getProductInfo(applyCd).getData();
        detail.setCustomer(customer);
        detail.setProduct(product);
        return detail;
    }
}
