package com.cango.ccs.service;

import com.cango.base.model.ApplyDetail;
import com.cango.base.model.ServiceInfo;

/**
 * @author liwenjie
 * @date 2021/10/15 10:09
 */
public interface CcsService {
    ServiceInfo sayHello();
    ApplyDetail getApplyDetail(String applyCd);
}
