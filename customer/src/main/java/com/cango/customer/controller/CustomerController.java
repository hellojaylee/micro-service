package com.cango.customer.controller;

import com.cango.base.model.BaseResponse;
import com.cango.base.model.CustomerEntity;
import com.cango.base.model.ServiceInfo;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

/**
 * @author liwenjie
 * @date 2021/10/29 14:34
 */
@RestController
@RequestMapping("/customer")
public class CustomerController {
    @GetMapping("/info")
    public BaseResponse<CustomerEntity> getCustomerInfo(@RequestParam("applyCd")String applyCd){
        CustomerEntity customer = new CustomerEntity("310000196808278888","张三",59,"13612345678","野生动物园");
        return new BaseResponse(customer);
    }
}
