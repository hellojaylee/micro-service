package com.cango.gateway.filter;

import com.cango.base.model.ErrorEnum;
import com.cango.gateway.util.ResponseUtil;
import org.apache.commons.lang3.StringUtils;
import org.springframework.cloud.gateway.filter.GatewayFilterChain;
import org.springframework.cloud.gateway.filter.GlobalFilter;
import org.springframework.core.Ordered;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.server.reactive.ServerHttpResponse;
import org.springframework.stereotype.Component;
import org.springframework.web.server.ServerWebExchange;
import reactor.core.publisher.Mono;

/**
 * @Author: Ajax
 * @Date: 2021/3/18 12:16
 */
@Component
public class TokenFilter implements GlobalFilter, Ordered {

    @Override
    public Mono<Void> filter(ServerWebExchange exchange, GatewayFilterChain chain) {

        if (needCheck(exchange.getRequest().getURI().getPath())) {
            // 验证token是否有效
            ServerHttpResponse resp = exchange.getResponse();
            HttpHeaders header = exchange.getRequest().getHeaders();
            String token = header.getFirst("token");
            if(StringUtils.isEmpty(token)){
                return unauthorized(resp, ErrorEnum.NO_AUTH.getCode(),"token校验失败");
            }
        }
        return chain.filter(exchange);
    }

    /**
     * 检查是否忽略url
     * @param path 路径
     * @return boolean
     */
    private boolean needCheck(String path) {
        return true;
    }


    /**
     * 移除模块前缀
     * @return String
     */
    private String replacePrefix(String path) {
        if (path.startsWith("/mate")) {
            return path.substring(path.indexOf("/",1));
        }
        return path;
    }

    private Mono<Void> unauthorized(ServerHttpResponse resp,String code, String msg) {
        return ResponseUtil.webFluxResponseWriter(resp, "application/json;charset=UTF-8", HttpStatus.OK,code, msg);
    }

    @Override
    public int getOrder() {
        return 0;
    }
}
