package com.cango.gateway.conf;

import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.cloud.gateway.filter.factory.AbstractGatewayFilterFactory;
import org.springframework.cloud.gateway.filter.ratelimit.KeyResolver;
import org.springframework.cloud.gateway.filter.ratelimit.RateLimiter;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Primary;
import org.springframework.data.redis.core.ReactiveRedisTemplate;
import org.springframework.data.redis.core.script.RedisScript;
import org.springframework.stereotype.Component;
import reactor.core.publisher.Mono;

import javax.xml.validation.Validator;
import java.util.List;


/**
 * @description: 
 * @author: liwenjie
 * @time: 2021/10/28 14:39
 */
@Configuration
public class RateLimiterConfiguration {

    @Bean(name="tokenKeyResolver")
    @Primary
    KeyResolver tokenKeyResolver() {
        //按IP来限流
        return exchange -> {
            String token  = exchange.getRequest().getHeaders().getFirst("token");
            if(token == null || token.isEmpty()){
                token = "token";
            }
            return  Mono.just(token);
        };
    }

}
